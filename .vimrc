execute pathogen#infect()
syntax on
filetype plugin indent on
set ts=4
set sw=4

function! StartUp()
	if 0 == argc()
		NERDTree
	end
endfunction

autocmd VimEnter * call StartUp()
